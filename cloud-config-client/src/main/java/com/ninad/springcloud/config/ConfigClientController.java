package com.ninad.springcloud.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigClientController {

	@Value("${spring.datasource.password}")
	private String password;

	@RequestMapping(value = "/showpassword", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public String showPassword() {
		return String.format("Decrypted password is <b>%s</b>.", password);
	}
}